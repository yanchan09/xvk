use std::io;

use log::debug;
use thiserror::Error;

use crate::{shm, xwin};

struct LocalSegment {
    segment: shm::Segment,
    mapped: shm::MappedSegment,
    size: usize,
}

impl LocalSegment {
    fn allocate(size: usize) -> io::Result<LocalSegment> {
        let segment = shm::Segment::create(size)?;
        let mapped = segment.map()?;
        Ok(LocalSegment {
            segment,
            mapped,
            size,
        })
    }
}

#[derive(Error, Debug)]
pub enum ShmError {
    #[error("io error: {0}")]
    IO(#[from] io::Error),
    #[error("X error: {0}")]
    X(#[from] xcb::Error),
}

type Result<T> = std::result::Result<T, ShmError>;

pub struct ShmManager {
    x: xwin::Connection,
    local: Option<LocalSegment>,
    remote: Option<xwin::ServerShmSegment>,
}

impl ShmManager {
    pub fn new(x: xwin::Connection, initial_size: usize) -> Result<ShmManager> {
        debug!("Creating shm with size {}", initial_size);
        let local = LocalSegment::allocate(initial_size)?;
        let remote = x.map_shm(local.segment.shm_id() as u32)?;
        Ok(ShmManager {
            x,
            local: Some(local),
            remote: Some(remote),
        })
    }

    pub fn ensure_size(&mut self, size: usize) -> Result<()> {
        if size == 0 {
            return Ok(());
        }

        let current_size = self.local.as_ref().unwrap().size;
        if current_size < size {
            debug!("Resizing shm ({} -> {})", current_size, size * 2);
            self.remote = None;
            self.local = None;
            let local = LocalSegment::allocate(size * 2)?;
            let remote = self.x.map_shm(local.segment.shm_id() as u32)?;
            self.local = Some(local);
            self.remote = Some(remote);
        }

        Ok(())
    }

    pub fn x(&self) -> &xwin::ServerShmSegment {
        self.remote.as_ref().unwrap()
    }

    pub fn contents(&self, len: usize) -> &[u8] {
        &self.local.as_ref().unwrap().mapped.as_slice()[0..len]
    }
}
