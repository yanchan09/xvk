use std::fmt::Debug;
use std::sync::{Arc};

use crate::XSurfaceImage;

pub struct TreeRenderObject {
    pub position: [i32; 3],
    pub size: [u32; 2],
}

impl Debug for TreeRenderObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TreeRenderObject")
            .field("position", &self.position)
            .field("size", &self.size)
            .finish()
    }
}

#[derive(Debug)]
pub enum ObjectSurface {
    X(Arc<XSurfaceImage>),
}

#[derive(Debug)]
pub enum TreeEvent {
    UpdateRenderObject(u64, TreeRenderObject),
    RemoveRenderObject(u64),
    ZUpdate(Vec<u64>),
    UpdateSurface(u64, ObjectSurface),
}
