use core::slice;
use std::io::{Error, Result};
use std::ptr;

use libc::c_void;
use log::debug;

static P_SHM_SEGMENT: &[u8] = b"ShmSegment\0";

pub struct Segment {
    id: i32,
    size: usize,
}

impl Segment {
    pub fn create(size: usize) -> Result<Segment> {
        let id = unsafe { libc::shmget(libc::IPC_PRIVATE, size, libc::IPC_CREAT | 0o600) };
        if id < 0 {
            Err(Error::last_os_error())
        } else {
            unsafe {
                tracy_client::sys::___tracy_emit_memory_alloc_named(
                    id as *const _,
                    size,
                    0,
                    P_SHM_SEGMENT.as_ptr() as *const i8,
                );
            }
            Ok(Segment { id, size })
        }
    }

    pub fn map(&self) -> Result<MappedSegment> {
        let addr = unsafe { libc::shmat(self.id, ptr::null(), 0) };
        if (addr as isize) < 0 {
            Err(Error::last_os_error())
        } else {
            Ok(MappedSegment {
                addr,
                size: self.size,
            })
        }
    }

    pub fn shm_id(&self) -> i32 {
        self.id
    }

    pub fn shm_size(&self) -> usize {
        self.size
    }
}

impl Drop for Segment {
    fn drop(&mut self) {
        debug!("Dropping Segment {}", self.id);
        unsafe {
            libc::shmctl(self.id, libc::IPC_RMID, ptr::null_mut());
            tracy_client::sys::___tracy_emit_memory_free_named(
                self.id as *const _,
                0,
                P_SHM_SEGMENT.as_ptr() as *const i8,
            );
        }
    }
}

pub struct MappedSegment {
    addr: *const c_void,
    size: usize,
}

impl<'a> MappedSegment {
    pub fn as_slice(&'a self) -> &'a [u8] {
        unsafe { slice::from_raw_parts(self.addr as *const u8, self.size) }
    }

    pub fn as_mut_slice(&'a mut self) -> &'a mut [u8] {
        unsafe { slice::from_raw_parts_mut(self.addr as *mut u8, self.size) }
    }
}

impl Drop for MappedSegment {
    fn drop(&mut self) {
        debug!("Umapping MappedSegment at {:p}", self.addr);
        unsafe {
            libc::shmdt(self.addr);
        }
    }
}

unsafe impl Send for MappedSegment {}
