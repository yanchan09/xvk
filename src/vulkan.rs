use std::{
    collections::BTreeMap,
    error::Error,
    sync::{
        atomic::{AtomicBool, Ordering},
        mpsc::Receiver,
        Arc,
    },
    thread::JoinHandle,
};

use log::{debug, error, info};
use tracy_client::span;
use vulkano::{
    buffer::{Buffer, BufferContents, BufferCreateInfo, BufferUsage, Subbuffer},
    command_buffer::{
        allocator::StandardCommandBufferAllocator, AutoCommandBufferBuilder, CommandBufferUsage,
        CopyBufferToImageInfo, PrimaryAutoCommandBuffer, RenderPassBeginInfo, SubpassBeginInfo,
        SubpassContents, SubpassEndInfo,
    },
    descriptor_set::{
        allocator::StandardDescriptorSetAllocator, DescriptorSet, PersistentDescriptorSet,
        WriteDescriptorSet,
    },
    device::{
        physical::PhysicalDevice, Device, DeviceCreateInfo, DeviceExtensions, DeviceOwned, Queue,
        QueueCreateInfo, QueueFlags,
    },
    format::Format,
    image::{
        sampler::{Filter, Sampler, SamplerCreateInfo},
        view::{ImageView, ImageViewCreateInfo},
        Image, ImageAspects, ImageCreateInfo, ImageLayout, ImageSubresourceRange, ImageUsage,
    },
    instance::{Instance, InstanceCreateInfo, InstanceExtensions},
    memory::{
        allocator::{
            AllocationCreateInfo, AllocationType, DeviceLayout, MemoryAlloc, MemoryAllocator,
            MemoryAllocatorError, MemoryTypeFilter, StandardMemoryAllocator,
        },
        DedicatedAllocation, ExternalMemoryHandleTypes, MemoryRequirements,
    },
    pipeline::{
        graphics::{
            color_blend::{
                AttachmentBlend, BlendFactor, BlendOp, ColorBlendAttachmentState, ColorBlendState,
            },
            input_assembly::{InputAssemblyState, PrimitiveTopology},
            multisample::MultisampleState,
            rasterization::RasterizationState,
            vertex_input::{Vertex, VertexDefinition, VertexInputState},
            viewport::{Viewport, ViewportState},
            GraphicsPipelineCreateInfo,
        },
        layout::PipelineDescriptorSetLayoutCreateInfo,
        GraphicsPipeline, Pipeline, PipelineBindPoint, PipelineLayout,
        PipelineShaderStageCreateInfo,
    },
    render_pass::{Framebuffer, FramebufferCreateInfo, RenderPass, Subpass},
    swapchain::{self, PresentMode, Surface, Swapchain, SwapchainCreateInfo, SwapchainPresentInfo},
    sync::{self, GpuFuture},
    Validated, VulkanError, VulkanLibrary,
};

static P_VULKAN_MEMORY: &[u8] = b"VulkanMemory\0";

use crate::{
    tree::{ObjectSurface, TreeEvent, TreeRenderObject},
    xwin,
};

pub struct TracedMemoryAllocator {
    inner: Box<dyn MemoryAllocator>,
}

impl TracedMemoryAllocator {
    pub fn wrap<A>(allocator: A) -> Self
    where
        A: MemoryAllocator,
    {
        Self {
            inner: Box::new(allocator),
        }
    }
}

unsafe impl DeviceOwned for TracedMemoryAllocator {
    fn device(&self) -> &Arc<Device> {
        self.inner.device()
    }
}

unsafe impl MemoryAllocator for TracedMemoryAllocator {
    fn find_memory_type_index(
        &self,
        memory_type_bits: u32,
        filter: MemoryTypeFilter,
    ) -> Option<u32> {
        self.inner.find_memory_type_index(memory_type_bits, filter)
    }

    fn allocate_from_type(
        &self,
        memory_type_index: u32,
        layout: DeviceLayout,
        allocation_type: AllocationType,
        never_allocate: bool,
    ) -> Result<MemoryAlloc, MemoryAllocatorError> {
        self.inner
            .allocate_from_type(memory_type_index, layout, allocation_type, never_allocate)
            .map(|alloc| {
                println!(
                    "alloc {}",
                    alloc
                        .suballocation
                        .map_or_else(|| alloc.allocation_handle, |sa| sa.handle)
                        .as_index()
                );
                unsafe {
                    tracy_client::sys::___tracy_emit_memory_alloc_named(
                        alloc.allocation_handle.as_ptr() as *const _,
                        alloc.device_memory.allocation_size() as usize,
                        0,
                        P_VULKAN_MEMORY.as_ptr() as *const i8,
                    );
                }
                alloc
            })
    }

    fn allocate(
        &self,
        requirements: MemoryRequirements,
        allocation_type: AllocationType,
        create_info: AllocationCreateInfo,
        dedicated_allocation: Option<DedicatedAllocation<'_>>,
    ) -> Result<MemoryAlloc, MemoryAllocatorError> {
        self.inner
            .allocate(
                requirements,
                allocation_type,
                create_info,
                dedicated_allocation,
            )
            .map(|alloc| {
                println!(
                    "alloc {}",
                    alloc
                        .suballocation
                        .map_or_else(|| alloc.allocation_handle, |sa| sa.handle)
                        .as_index()
                );
                unsafe {
                    tracy_client::sys::___tracy_emit_memory_alloc_named(
                        alloc.allocation_handle.as_ptr() as *const _,
                        alloc.device_memory.allocation_size() as usize,
                        0,
                        P_VULKAN_MEMORY.as_ptr() as *const i8,
                    );
                }
                alloc
            })
    }

    fn allocate_dedicated(
        &self,
        memory_type_index: u32,
        allocation_size: vulkano::DeviceSize,
        dedicated_allocation: Option<DedicatedAllocation<'_>>,
        export_handle_types: ExternalMemoryHandleTypes,
    ) -> Result<MemoryAlloc, MemoryAllocatorError> {
        self.inner
            .allocate_dedicated(
                memory_type_index,
                allocation_size,
                dedicated_allocation,
                export_handle_types,
            )
            .map(|alloc| {
                println!(
                    "alloc {}",
                    alloc
                        .suballocation
                        .map_or_else(|| alloc.allocation_handle, |sa| sa.handle)
                        .as_index()
                );
                unsafe {
                    tracy_client::sys::___tracy_emit_memory_alloc_named(
                        alloc.allocation_handle.as_ptr() as *const _,
                        alloc.device_memory.allocation_size() as usize,
                        0,
                        P_VULKAN_MEMORY.as_ptr() as *const i8,
                    );
                }
                alloc
            })
    }

    unsafe fn deallocate(&self, allocation: MemoryAlloc) {
        println!("free {}", allocation.allocation_handle.as_index());
        unsafe {
            tracy_client::sys::___tracy_emit_memory_free_named(
                allocation.allocation_handle.as_ptr() as *const _,
                0,
                P_VULKAN_MEMORY.as_ptr() as *const i8,
            );
        }
        self.inner.deallocate(allocation);
    }
}

mod vs {
    vulkano_shaders::shader! {
        ty: "vertex",
        src: r"
            #version 460

            vec2 v_position[] = {
                vec2(1.0, 0.0),
                vec2(0.0, 0.0),
                vec2(1.0, 1.0),
                vec2(0.0, 1.0)
            };

            layout(location = 0) in vec2 u_position;
            layout(location = 1) in vec2 u_extent;

            layout(push_constant) uniform constants
            {
                vec2 view_extent;
            } PushConstants;

            layout(location = 0) out vec2 fs_texcoord;

            void main() {
                vec2 scale = u_extent / PushConstants.view_extent;
                vec2 offset = u_position / PushConstants.view_extent;
                vec2 final_vtx = offset + v_position[gl_VertexIndex] * scale;
                // Vertex in range [0.0, 1.0], need to map to [-1.0, 1.0] NDC space.
                gl_Position = vec4(final_vtx * 2 - vec2(1.0, 1.0), 0.0, 1.0);
                fs_texcoord = v_position[gl_VertexIndex];
            }
        "
    }
}

mod fs {
    vulkano_shaders::shader! {
        ty: "fragment",
        src: r"
            #version 460

            layout(location = 0) in vec2 fs_texcoord;
            layout(set = 0, binding = 0) uniform sampler2D u_tex;

            layout(location = 0) out vec4 f_color;

            void main() {
                f_color = texture(u_tex, fs_texcoord);
            }
        "
    }
}

#[derive(BufferContents, Vertex, Debug)]
#[repr(C)]
struct VkInstance {
    #[format(R32G32_SFLOAT)]
    #[name("u_position")]
    position: [f32; 2],
    #[format(R32G32_SFLOAT)]
    #[name("u_extent")]
    extent: [f32; 2],
}

#[derive(BufferContents)]
#[repr(C)]
struct VkPushConstants {
    view_extent: [f32; 2],
}

type VkResult<T> = Result<T, vulkano::Validated<vulkano::VulkanError>>;

struct VkRenderObjectState {
    buffer: Subbuffer<[u8]>,
    image: Arc<Image>,
    image_view: Arc<ImageView>,
    position: [i32; 3],
    size: [u32; 2],
    instance: Option<u32>,
    surface: Option<ObjectSurface>,
}

struct StaticPipelineLayout {
    vertex_input_state: VertexInputState,
    vs_stage: PipelineShaderStageCreateInfo,
    fs_stage: PipelineShaderStageCreateInfo,
    layout: Arc<PipelineLayout>,
}

impl StaticPipelineLayout {
    fn for_device(device: &Arc<Device>) -> VkResult<StaticPipelineLayout> {
        let vs = vs::load(device.clone())?.entry_point("main").unwrap();
        let fs = fs::load(device.clone())?.entry_point("main").unwrap();

        let vertex_input_state =
            VkInstance::per_instance().definition(&vs.info().input_interface)?;

        let vs_stage = PipelineShaderStageCreateInfo::new(vs);
        let fs_stage = PipelineShaderStageCreateInfo::new(fs);

        let layout = PipelineLayout::new(
            device.clone(),
            PipelineDescriptorSetLayoutCreateInfo::from_stages([&vs_stage, &fs_stage])
                .into_pipeline_layout_create_info(device.clone())
                .unwrap(),
        )?;

        Ok(StaticPipelineLayout {
            vertex_input_state,
            vs_stage,
            fs_stage,
            layout,
        })
    }

    fn pipeline_create_info(&self) -> GraphicsPipelineCreateInfo {
        GraphicsPipelineCreateInfo {
            stages: [self.vs_stage.clone(), self.fs_stage.clone()]
                .into_iter()
                .collect(),
            vertex_input_state: Some(self.vertex_input_state.clone()),
            ..GraphicsPipelineCreateInfo::layout(self.layout.clone())
        }
    }
}

pub struct Renderer {
    surface: Arc<Surface>,
    physical_device: Arc<PhysicalDevice>,
    device: Arc<Device>,
    queue: Arc<Queue>,
    instance_buffer: Option<Subbuffer<[VkInstance]>>,
    texture_sampler: Arc<Sampler>,
    render_objs: Receiver<TreeEvent>,
    render_obj_state: BTreeMap<u64, VkRenderObjectState>,
    allocator: Arc<dyn MemoryAllocator>,
    pipeline_layout: StaticPipelineLayout,
}

impl Renderer {
    pub fn for_xwin<W>(
        window: Arc<W>,
        render_objs: Receiver<TreeEvent>,
    ) -> Result<Renderer, Box<dyn std::error::Error>>
    where
        W: xwin::Window + 'static,
    {
        let library = VulkanLibrary::new()?;
        let instance = Instance::new(
            library,
            InstanceCreateInfo {
                enabled_extensions: InstanceExtensions {
                    khr_xcb_surface: true,
                    ..Default::default()
                },
                ..Default::default()
            },
        )?;

        let surface = unsafe {
            Surface::from_xcb(
                instance.clone(),
                window.xcb_conn().get_raw_conn(),
                window.xid(),
                Some(window.clone()),
            )?
        };

        info!("Created Vulkan surface");

        let physical_device = instance.enumerate_physical_devices()?.next().unwrap();

        let dev_props = physical_device.properties();

        info!("Physical device: {}", dev_props.device_name);

        let queue_family_index = physical_device
            .queue_family_properties()
            .iter()
            .enumerate()
            .position(|(i, q)| {
                q.queue_flags.contains(QueueFlags::GRAPHICS)
                    && physical_device
                        .surface_support(i as u32, &surface)
                        .unwrap_or(false)
            })
            .unwrap();
        let (device, queues) = Device::new(
            physical_device.clone(),
            DeviceCreateInfo {
                queue_create_infos: vec![QueueCreateInfo {
                    queue_family_index: queue_family_index as u32,
                    ..Default::default()
                }],
                enabled_extensions: DeviceExtensions {
                    khr_swapchain: true,
                    ..DeviceExtensions::empty()
                },
                ..Default::default()
            },
        )?;

        let allocator: Arc<dyn MemoryAllocator> =
            Arc::new(StandardMemoryAllocator::new_default(device.clone()));

        let render_obj_state = BTreeMap::new();

        let texture_sampler = Sampler::new(
            device.clone(),
            SamplerCreateInfo {
                mag_filter: Filter::Linear,
                min_filter: Filter::Linear,
                ..Default::default()
            },
        )?;

        let pipeline_layout = StaticPipelineLayout::for_device(&device)?;

        let queue = queues.into_iter().next().unwrap();

        Ok(Renderer {
            surface,
            physical_device,
            device,
            queue,
            instance_buffer: None,
            texture_sampler,
            render_objs,
            render_obj_state,
            allocator,
            pipeline_layout,
        })
    }

    pub fn run(mut self) -> RendererRunGuard {
        let over = Arc::new(AtomicBool::new(false));
        let rt_over = over.clone();
        let join_handle = std::thread::spawn(move || {
            tracy_client::set_thread_name!("VulkanRenderLoop");
            match self.render_loop(rt_over) {
                Err(e) => error!("Render thread exited: {:?}", e),
                _ => {}
            }
        });
        RendererRunGuard {
            join_handle: Some(join_handle),
            over,
        }
    }

    fn render_loop(&mut self, over: Arc<AtomicBool>) -> Result<(), Box<dyn std::error::Error>> {
        'outer: loop {
            let caps = self
                .physical_device
                .surface_capabilities(&self.surface, Default::default())?;
            let surface_formats = self
                .physical_device
                .surface_formats(&self.surface, Default::default())?;
            info!("Supported surface formats:");
            for format in surface_formats.iter() {
                info!("- {:?}", format);
            }
            let image_format = surface_formats.first().unwrap().0;

            info!("Surface capabilities: {:?}", caps);
            info!("Swapchain image format: {:?}", image_format);

            let (swapchain, images) = Swapchain::new(
                self.device.clone(),
                self.surface.clone(),
                SwapchainCreateInfo {
                    min_image_count: caps.min_image_count,
                    image_format,
                    image_extent: caps.max_image_extent,
                    image_usage: ImageUsage::COLOR_ATTACHMENT,
                    present_mode: PresentMode::Fifo,
                    ..Default::default()
                },
            )?;

            let render_pass: Arc<RenderPass> = self.create_render_pass(&swapchain)?;
            let framebuffers = self.create_framebuffers(&images, &render_pass)?;
            let allocator =
                StandardCommandBufferAllocator::new(self.device.clone(), Default::default());
            let pipeline = self.create_pipeline(&render_pass, caps.max_image_extent)?;
            let mut descriptor_sets = self.create_descriptor_sets(&pipeline)?;

            loop {
                if over.load(Ordering::SeqCst) {
                    return Ok(());
                }

                let mut descriptor_sets_stale = false;
                let mut uniforms_stale = false;

                for event in self.render_objs.try_iter() {
                    let _span = span!("VulkanRenderer::handleRenderCommand");
                    debug!("TreeEvent: {:?}", event);
                    match event {
                        TreeEvent::UpdateRenderObject(id, info) => {
                            match self.render_obj_state.get_mut(&id) {
                                Some(obj) => {
                                    if obj.size == info.size {
                                        obj.position = info.position;
                                        uniforms_stale = true;
                                    } else {
                                        self.render_obj_state.insert(
                                            id,
                                            create_state_for_renderobject(&self.allocator, info)?,
                                        );
                                        descriptor_sets_stale = true;
                                        uniforms_stale = true;
                                    }
                                }
                                None => {
                                    debug!("New RenderObject {}", id);
                                    self.render_obj_state.insert(
                                        id,
                                        create_state_for_renderobject(&self.allocator, info)?,
                                    );
                                    descriptor_sets_stale = true;
                                    uniforms_stale = true;
                                }
                            }
                        }
                        TreeEvent::RemoveRenderObject(id) => {
                            self.render_obj_state.remove(&id);
                            descriptor_sets_stale = true;
                            uniforms_stale = true;
                        }
                        TreeEvent::ZUpdate(z_list) => {
                            for (i, id) in z_list.into_iter().enumerate() {
                                if let Some(obj) = self.render_obj_state.get_mut(&id) {
                                    obj.position[2] = i as i32;
                                }
                            }
                            let mut instances: Vec<(u64, &VkRenderObjectState)> =
                                self.render_obj_state.iter().map(|(k, v)| (*k, v)).collect();
                            instances.sort_by(|a, b| a.1.position[2].cmp(&b.1.position[2]));
                            let stack: Vec<u64> = instances.iter().map(|(id, _)| *id).collect();
                            debug!("Updated Z-stack: {:?}", stack);
                        }
                        TreeEvent::UpdateSurface(id, surface) => {
                            if let Some(obj) = self.render_obj_state.get_mut(&id) {
                                obj.surface = Some(surface);
                            }
                        }
                    }
                }

                for (_id, obj) in self.render_obj_state.iter_mut() {
                    let _span = span!("VulkanRenderer::updateSurface");
                    match &obj.surface {
                        Some(ObjectSurface::X(surface)) => {
                            let mut wr = obj.buffer.write()?;
                            surface.lock(|buf| wr.copy_from_slice(buf));
                        }
                        _ => {}
                    }
                }

                if descriptor_sets_stale {
                    let _span = span!("VulkanRenderer::createDescriptorSets");
                    descriptor_sets = self.create_descriptor_sets(&pipeline)?;
                }

                if self.instance_buffer.as_ref().map(|b| b.len()).unwrap_or(0)
                    < self.render_obj_state.len() as u64
                {
                    let _span = span!("VulkanRenderer::allocateInstanceBuffer");
                    debug!("Allocating new instance buffer");
                    self.instance_buffer = Some(Buffer::new_slice(
                        self.allocator.clone(),
                        BufferCreateInfo {
                            usage: BufferUsage::VERTEX_BUFFER,
                            ..Default::default()
                        },
                        AllocationCreateInfo {
                            memory_type_filter: MemoryTypeFilter::PREFER_DEVICE
                                | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
                            ..Default::default()
                        },
                        self.render_obj_state.len() as u64,
                    )?);
                    uniforms_stale = true;
                }
                if uniforms_stale && self.render_obj_state.len() > 0 {
                    let _span = span!("VulkanRenderer::updateInstanceBuffer");
                    let mut instance_buffer = self.instance_buffer.as_mut().unwrap().write()?;
                    for (i, (id, obj)) in self.render_obj_state.iter_mut().enumerate() {
                        let instance = VkInstance {
                            position: [obj.position[0] as f32, obj.position[1] as f32],
                            extent: [obj.size[0] as f32, obj.size[1] as f32],
                        };
                        debug!("Instance for {}: {:?}", id, instance);
                        instance_buffer[i] = instance;
                        obj.instance = Some(i as u32);
                    }
                }

                let command_buffers = self.create_command_buffers(
                    &allocator,
                    &framebuffers,
                    &pipeline,
                    &descriptor_sets,
                )?;

                let mut builder = AutoCommandBufferBuilder::primary(
                    &allocator,
                    self.queue.queue_family_index(),
                    CommandBufferUsage::OneTimeSubmit,
                )?;
                for state in self.render_obj_state.values() {
                    builder.copy_buffer_to_image(CopyBufferToImageInfo {
                        dst_image_layout: ImageLayout::TransferDstOptimal,
                        ..CopyBufferToImageInfo::buffer_image(
                            state.buffer.clone(),
                            state.image.clone(),
                        )
                    })?;
                }

                let _span = span!("VulkanRenderer::gpuRender");
                let (image_idx, _suboptimal, acquire_future) =
                    swapchain::acquire_next_image(swapchain.clone(), None)?;
                let execution = sync::now(self.device.clone())
                    .then_execute(self.queue.clone(), builder.build()?)?
                    .then_signal_semaphore_and_flush()?
                    .join(acquire_future)
                    .then_execute(
                        self.queue.clone(),
                        command_buffers[image_idx as usize].clone(),
                    )?
                    .then_swapchain_present(
                        self.queue.clone(),
                        SwapchainPresentInfo::swapchain_image_index(swapchain.clone(), image_idx),
                    )
                    .then_signal_fence_and_flush();
                match execution.map_err(Validated::unwrap) {
                    Err(VulkanError::OutOfDate) => continue 'outer,
                    Ok(fut) => fut.wait(None)?,
                    Err(e) => return Err(Box::new(e)),
                }
                tracy_client::frame_mark();
            }
        }
    }

    fn create_render_pass(&self, swapchain: &Arc<Swapchain>) -> VkResult<Arc<RenderPass>> {
        vulkano::single_pass_renderpass!(
            self.device.clone(),
            attachments: {
                color: {
                    format: swapchain.image_format(),
                    samples: 1,
                    load_op: Clear,
                    store_op: Store
                },
            },
            pass: {
                color: [color],
                depth_stencil: {}
            }
        )
    }

    fn create_framebuffers(
        &self,
        images: &[Arc<Image>],
        render_pass: &Arc<RenderPass>,
    ) -> VkResult<Vec<Arc<Framebuffer>>> {
        images
            .iter()
            .map(|image| {
                let view = ImageView::new_default(image.clone()).unwrap();
                Framebuffer::new(
                    render_pass.clone(),
                    FramebufferCreateInfo {
                        attachments: vec![view],
                        ..Default::default()
                    },
                )
            })
            .collect()
    }

    fn create_command_buffers(
        &self,
        allocator: &StandardCommandBufferAllocator,
        framebuffers: &[Arc<Framebuffer>],
        pipeline: &Arc<GraphicsPipeline>,
        descriptor_sets: &BTreeMap<u64, Arc<PersistentDescriptorSet>>,
    ) -> VkResult<Vec<Arc<PrimaryAutoCommandBuffer>>> {
        framebuffers
            .iter()
            .map(|framebuffer| {
                let mut builder = AutoCommandBufferBuilder::primary(
                    allocator,
                    self.queue.queue_family_index(),
                    CommandBufferUsage::MultipleSubmit,
                )?;
                builder
                    .begin_render_pass(
                        RenderPassBeginInfo {
                            clear_values: vec![Some([0.0, 0.0, 0.0, 1.0].into())],
                            ..RenderPassBeginInfo::framebuffer(framebuffer.clone())
                        },
                        SubpassBeginInfo {
                            contents: SubpassContents::Inline,
                            ..Default::default()
                        },
                    )?
                    .bind_pipeline_graphics(pipeline.clone())?;
                if let Some(instance_buffer) = &self.instance_buffer {
                    builder.bind_vertex_buffers(0, instance_buffer.clone())?;
                }
                let mut instances: Vec<(u64, &VkRenderObjectState)> =
                    self.render_obj_state.iter().map(|(k, v)| (*k, v)).collect();
                instances.sort_by(|a, b| a.1.position[2].cmp(&b.1.position[2]));
                for (id, state) in instances {
                    let ds = descriptor_sets.get(&id).unwrap();
                    builder
                        .bind_descriptor_sets(
                            PipelineBindPoint::Graphics,
                            pipeline.layout().clone(),
                            0,
                            ds.clone(),
                        )?
                        .push_constants(
                            pipeline.layout().clone(),
                            0,
                            VkPushConstants {
                                view_extent: [3840.0, 1080.0],
                            },
                        )?
                        .draw(4, 1, 0, state.instance.unwrap())?;
                }
                builder.end_render_pass(SubpassEndInfo::default())?;
                Ok(builder.build()?)
            })
            .collect()
    }

    fn create_pipeline(
        &self,
        render_pass: &Arc<RenderPass>,
        extent: [u32; 2],
    ) -> VkResult<Arc<GraphicsPipeline>> {
        let subpass = Subpass::from(render_pass.clone(), 0).unwrap();

        let pipeline = GraphicsPipeline::new(
            self.device.clone(),
            None,
            GraphicsPipelineCreateInfo {
                input_assembly_state: Some(InputAssemblyState {
                    topology: PrimitiveTopology::TriangleStrip,
                    ..Default::default()
                }),
                viewport_state: Some(ViewportState {
                    viewports: [Viewport {
                        offset: [0.0, 0.0],
                        extent: [extent[0] as f32, extent[1] as f32],
                        depth_range: 0.0..=1.0,
                    }]
                    .into_iter()
                    .collect(),
                    ..Default::default()
                }),
                rasterization_state: Some(RasterizationState::default()),
                multisample_state: Some(MultisampleState::default()),
                color_blend_state: Some(ColorBlendState::with_attachment_states(
                    subpass.num_color_attachments(),
                    ColorBlendAttachmentState {
                        blend: Some(AttachmentBlend {
                            src_color_blend_factor: BlendFactor::SrcAlpha,
                            dst_color_blend_factor: BlendFactor::OneMinusSrcAlpha,
                            color_blend_op: BlendOp::Add,
                            src_alpha_blend_factor: BlendFactor::SrcAlpha,
                            dst_alpha_blend_factor: BlendFactor::OneMinusSrcAlpha,
                            alpha_blend_op: BlendOp::Subtract,
                        }),
                        ..Default::default()
                    },
                )),
                subpass: Some(subpass.into()),
                ..self.pipeline_layout.pipeline_create_info()
            },
        )?;

        Ok(pipeline)
    }

    fn create_descriptor_sets(
        &self,
        pipeline: &Arc<GraphicsPipeline>,
    ) -> VkResult<BTreeMap<u64, Arc<PersistentDescriptorSet>>> {
        let allocator =
            StandardDescriptorSetAllocator::new(self.device.clone(), Default::default());
        self.render_obj_state
            .iter()
            .map(|(k, state)| {
                PersistentDescriptorSet::new(
                    &allocator,
                    pipeline.layout().set_layouts()[0].clone(),
                    [WriteDescriptorSet::image_view_sampler(
                        0,
                        state.image_view.clone(),
                        self.texture_sampler.clone(),
                    )],
                    [],
                )
                .map(|set| (*k, set))
            })
            .collect()
    }
}

pub struct RendererRunGuard {
    join_handle: Option<JoinHandle<()>>,
    over: Arc<AtomicBool>,
}

impl Drop for RendererRunGuard {
    fn drop(&mut self) {
        debug!("RendererRunGuard dropped, wrapping up!");
        self.over.store(true, Ordering::SeqCst);
        if let Some(join_handle) = self.join_handle.take() {
            let _ = join_handle.join();
        }
    }
}

fn create_state_for_renderobject(
    allocator: &Arc<dyn MemoryAllocator>,
    info: TreeRenderObject,
) -> Result<VkRenderObjectState, Box<dyn Error>> {
    let _span = span!("VulkanRenderer::create_state_for_renderobject");

    let _spani = span!("vulkano::Buffer::new_slice");
    let buffer = Buffer::new_slice(
        allocator.clone(),
        BufferCreateInfo {
            usage: BufferUsage::TRANSFER_SRC,
            ..Default::default()
        },
        AllocationCreateInfo {
            memory_type_filter: MemoryTypeFilter::PREFER_HOST
                | MemoryTypeFilter::HOST_SEQUENTIAL_WRITE,
            ..Default::default()
        },
        4 * (info.size[0] as u64) * (info.size[1] as u64),
    )?;
    drop(_spani);

    let _spani = span!("vulkano::Image::new");
    let image = Image::new(
        allocator.clone(),
        ImageCreateInfo {
            format: Format::B8G8R8A8_UNORM,
            extent: [info.size[0], info.size[1], 1],
            usage: ImageUsage::SAMPLED | ImageUsage::TRANSFER_DST,
            ..Default::default()
        },
        AllocationCreateInfo {
            memory_type_filter: MemoryTypeFilter::PREFER_DEVICE,
            ..Default::default()
        },
    )?;
    drop(_spani);

    let _spani = span!("vulkano::ImageView::new");
    let image_view = ImageView::new(
        image.clone(),
        ImageViewCreateInfo {
            format: image.format(),
            subresource_range: ImageSubresourceRange {
                aspects: ImageAspects::COLOR,
                mip_levels: 0..1,
                array_layers: 0..1,
            },
            ..Default::default()
        },
    )?;
    drop(_spani);

    Ok(VkRenderObjectState {
        buffer,
        image,
        image_view,
        position: info.position,
        size: info.size,
        surface: None,
        instance: None,
    })
}
