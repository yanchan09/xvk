use std::{fmt::Debug, ops::Deref, sync::Arc};

use log::debug;
use tracy_client::span;
use xcb::{composite, damage, shm as xshm, x, Extension, Xid};

#[derive(Clone)]
pub struct Connection {
    conn: Arc<xcb::Connection>,
    screen: i32,
}

impl Connection {
    pub fn connect() -> xcb::Result<Connection> {
        let (conn, screen) = xcb::Connection::connect_with_extensions(
            None,
            &[Extension::Shm, Extension::Composite, Extension::Damage],
            &[],
        )?;
        Ok(Connection {
            conn: Arc::new(conn),
            screen,
        })
    }

    fn xcb_conn(&self) -> Arc<xcb::Connection> {
        self.conn.clone()
    }

    pub fn root_window(&self) -> x::Window {
        let setup = self.get_setup();
        let screen = setup.roots().nth(self.screen as usize).unwrap();
        return screen.root();
    }

    pub fn create_window(&self) -> xcb::Result<CreatedWindow> {
        let setup = self.get_setup();
        let screen = setup.roots().nth(self.screen as usize).unwrap();
        let window: x::Window = self.generate_id();
        self.send_and_check_request(&x::CreateWindow {
            depth: 24,
            wid: window,
            parent: screen.root(),
            x: 0,
            y: 0,
            width: 3840 / 2,
            height: 1080 / 2,
            border_width: 0,
            class: x::WindowClass::InputOutput,
            visual: screen.root_visual(),
            value_list: &[
                x::Cw::BackPixel(screen.white_pixel()),
                x::Cw::EventMask(x::EventMask::EXPOSURE),
            ],
        })?;
        let (wm_protocols, wm_delete_window) = (
            self.conn
                .wait_for_reply(self.send_request(&x::InternAtom {
                    only_if_exists: true,
                    name: b"WM_PROTOCOLS",
                }))?
                .atom(),
            self.conn
                .wait_for_reply(self.send_request(&x::InternAtom {
                    only_if_exists: true,
                    name: b"WM_DELETE_WINDOW",
                }))?
                .atom(),
        );
        self.send_and_check_request(&x::ChangeProperty {
            mode: x::PropMode::Replace,
            window,
            property: wm_protocols,
            r#type: x::ATOM_ATOM,
            data: &[wm_delete_window],
        })?;
        self.send_and_check_request(&x::MapWindow { window })?;
        Ok(CreatedWindow {
            conn: self.clone(),
            id: window,
            wm_delete_window,
        })
    }

    pub fn overlay_window(&self) -> xcb::Result<CompositeOverlayWindow> {
        let setup = self.get_setup();
        let screen = setup.roots().nth(self.screen as usize).unwrap();
        let cookie = self.send_request(&composite::GetOverlayWindow {
            window: screen.root(),
        });
        let reply = self.wait_for_reply(cookie)?;
        Ok(CompositeOverlayWindow {
            conn: self.clone(),
            id: reply.overlay_win(),
        })
    }

    pub fn map_shm(&self, shm_id: u32) -> xcb::Result<ServerShmSegment> {
        let seg: xshm::Seg = self.generate_id();

        debug!("Attaching shmid {} to XID {}", shm_id, seg.resource_id());
        self.send_and_check_request(&xshm::Attach {
            shmseg: seg,
            shmid: shm_id,
            read_only: false,
        })?;

        Ok(ServerShmSegment {
            xcb: self.clone(),
            seg,
        })
    }

    pub fn create_damage(&self, window: x::Window) -> xcb::Result<DamageRef> {
        let id: damage::Damage = self.generate_id();

        self.send_and_check_request(&damage::Create {
            damage: id,
            drawable: x::Drawable::Window(window),
            level: damage::ReportLevel::NonEmpty,
        })?;

        Ok(DamageRef {
            xcb: self.clone(),
            damage: id,
        })
    }

    // xcb crate traced wrappers

    pub fn send_request<R>(&self, req: &R) -> R::Cookie
    where
        R: xcb::Request + Debug,
    {
        let span = span!("xcb::send_request");
        span.emit_text(&format!("{:?}", req));
        self.conn.send_request(req)
    }

    pub fn send_request_checked<R>(&self, req: &R) -> xcb::VoidCookieChecked
    where
        R: xcb::RequestWithoutReply + Debug,
    {
        let span = span!("xcb::send_request_checked");
        span.emit_text(&format!("{:?}", req));
        self.conn.send_request_checked(req)
    }

    pub fn wait_for_reply<C>(&self, cookie: C) -> xcb::Result<C::Reply>
    where
        C: xcb::CookieWithReplyChecked + Debug,
    {
        let span = span!("xcb::wait_for_reply");
        span.emit_text(&format!("{:?}", cookie));
        self.conn.wait_for_reply(cookie)
    }

    pub fn check_request(&self, cookie: xcb::VoidCookieChecked) -> xcb::ProtocolResult<()> {
        let _span = span!("xcb::check_request");
        self.conn.check_request(cookie)
    }

    pub fn send_and_check_request<R>(&self, req: &R) -> xcb::ProtocolResult<()>
    where
        R: xcb::RequestWithoutReply + Debug,
    {
        let span = span!("xcb::send_and_check_request");
        span.emit_text(&format!("{:?}", req));
        self.check_request(self.send_request_checked(req))
    }

    pub fn poll_for_event(&self) -> xcb::Result<Option<xcb::Event>> {
        let _span = span!("xcb::poll_for_event");
        self.conn.poll_for_event()
    }

    pub fn flush(&self) -> xcb::ConnResult<()> {
        let _span = span!("xcb::flush");
        self.conn.flush()
    }

    pub fn generate_id<T>(&self) -> T
    where
        T: xcb::XidNew,
    {
        let _span = span!("xcb::generate_id");
        self.conn.generate_id()
    }

    pub fn get_setup(&self) -> &x::Setup {
        self.conn.get_setup()
    }
}

pub trait Window: Send + Sync {
    fn xcb_conn(&self) -> Arc<xcb::Connection>;
    fn xid(&self) -> u32;
}

pub struct CreatedWindow {
    conn: Connection,
    id: x::Window,
    wm_delete_window: x::Atom,
}

impl CreatedWindow {
    pub fn wm_delete_window_atom(&self) -> u32 {
        self.wm_delete_window.resource_id()
    }
}

impl Window for CreatedWindow {
    fn xcb_conn(&self) -> Arc<xcb::Connection> {
        self.conn.xcb_conn()
    }

    fn xid(&self) -> u32 {
        self.id.resource_id()
    }
}

impl Drop for CreatedWindow {
    fn drop(&mut self) {
        debug!("Releasing xwin::CreatedWindow({})", self.id.resource_id());
        self.conn
            .send_request(&x::DestroyWindow { window: self.id });
        let _ = self.conn.flush();
    }
}

pub struct CompositeOverlayWindow {
    conn: Connection,
    id: x::Window,
}

impl Window for CompositeOverlayWindow {
    fn xcb_conn(&self) -> Arc<xcb::Connection> {
        self.conn.xcb_conn()
    }

    fn xid(&self) -> u32 {
        self.id.resource_id()
    }
}

impl Drop for CompositeOverlayWindow {
    fn drop(&mut self) {
        debug!(
            "Releasing xwin::CompositeOverlayWindow({})",
            self.id.resource_id()
        );
        self.conn
            .send_request(&composite::ReleaseOverlayWindow { window: self.id });
        let _ = self.conn.flush();
    }
}

pub struct ServerShmSegment {
    xcb: Connection,
    seg: xshm::Seg,
}

impl ServerShmSegment {
    pub fn get_image(&self, drawable: x::Drawable, size: (u16, u16)) -> xcb::Result<()> {
        let cookie = self.xcb.send_request(&xshm::GetImage {
            shmseg: self.seg,
            drawable,
            x: 0,
            y: 0,
            width: size.0,
            height: size.1,
            plane_mask: u32::MAX,
            format: x::ImageFormat::ZPixmap as u8,
            offset: 0,
        });
        self.xcb.wait_for_reply(cookie)?;
        Ok(())
    }
}

impl Drop for ServerShmSegment {
    fn drop(&mut self) {
        self.xcb.send_request(&xshm::Detach { shmseg: self.seg });
        let _ = self.xcb.flush();
    }
}

pub struct DamageRef {
    xcb: Connection,
    damage: damage::Damage,
}

impl Debug for DamageRef {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DamageRef")
            .field("damage", &self.damage)
            .finish()
    }
}

impl Deref for DamageRef {
    type Target = damage::Damage;

    fn deref(&self) -> &Self::Target {
        &self.damage
    }
}

impl Drop for DamageRef {
    fn drop(&mut self) {
        self.xcb.send_request(&damage::Destroy {
            damage: self.damage,
        });
        let _ = self.xcb.flush();
    }
}
