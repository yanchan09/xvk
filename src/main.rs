use std::{
    collections::BTreeMap,
    fmt::Debug,
    sync::{mpsc, Arc, Mutex},
    thread::sleep,
    time::Duration,
};

use log::{debug, error, info, warn};
use tracy_client::{span, ProfiledAllocator};
use tree::ObjectSurface;
use xcb::{
    composite, damage,
    x::{self, Pixmap},
    xfixes, Xid,
};

use crate::{
    tree::{TreeEvent, TreeRenderObject},
    xwin::Window,
};

mod shm;
mod tree;
mod vulkan;
mod xshm;
mod xwin;

#[global_allocator]
static GLOBAL_ALLOC: ProfiledAllocator<std::alloc::System> =
    ProfiledAllocator::new(std::alloc::System, 0);

pub struct XSurfaceImage {
    xcb: xwin::Connection,
    shm: Arc<Mutex<xshm::ShmManager>>,

    size: (u16, u16),
    bufsz: usize,
    local_buf: Mutex<Vec<u8>>,

    pixmap: x::Pixmap,
}

impl Debug for XSurfaceImage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("XSurfaceImage")
            .field("size", &self.size)
            .field("pixmap", &self.pixmap)
            .finish()
    }
}

impl XSurfaceImage {
    fn for_window(
        xcb: xwin::Connection,
        shm: Arc<Mutex<xshm::ShmManager>>,
        window: x::Window,
        dimensions: (u16, u16),
    ) -> Result<Arc<XSurfaceImage>, Box<dyn std::error::Error>> {
        let _span = span!("XSurfaceImage::create");
        let buf_sz = (dimensions.0 as usize) * (dimensions.1 as usize) * 4;
        shm.lock().unwrap().ensure_size(buf_sz)?;
        debug!(
            "Created XSurfaceImage with dimensions {}x{}",
            dimensions.0, dimensions.1
        );

        let pixmap: Pixmap = xcb.generate_id();
        xcb.send_and_check_request(&composite::NameWindowPixmap { window, pixmap })?;

        let mut local_buf = Vec::new();
        local_buf.resize(buf_sz, 0);
        Ok(Arc::new(XSurfaceImage {
            xcb,
            shm,
            size: dimensions,
            pixmap,
            bufsz: buf_sz,
            local_buf: Mutex::new(local_buf),
        }))
    }

    pub fn lock<F>(&self, lock_fn: F)
    where
        F: FnOnce(&[u8]),
    {
        let buf = self.local_buf.lock().unwrap();
        lock_fn(&buf);
    }

    fn update(&self) -> Result<(), Box<dyn std::error::Error>> {
        let _span = span!("XSurfaceImage::update");
        let shm_lock = self.shm.lock().unwrap();
        shm_lock
            .x()
            .get_image(x::Drawable::Pixmap(self.pixmap), self.size)
            .map_err(|e| format!("Failed to getImage: {:?}", e))?;
        self.local_buf
            .lock()
            .unwrap()
            .copy_from_slice(shm_lock.contents(self.bufsz));
        Ok(())
    }
}

impl Drop for XSurfaceImage {
    fn drop(&mut self) {
        self.xcb.send_request(&x::FreePixmap {
            pixmap: self.pixmap,
        });
        let _ = self.xcb.flush();
    }
}

impl Into<ObjectSurface> for Arc<XSurfaceImage> {
    fn into(self) -> ObjectSurface {
        ObjectSurface::X(self)
    }
}

#[derive(Debug)]
struct TrackedXWin {
    position: (i32, i32),
    size: (u32, u32),
    class: x::WindowClass,
    map_state: x::MapState,
    visual: (u32, x::Visualtype),
    damage: Option<xwin::DamageRef>,
    surface: Option<Arc<XSurfaceImage>>,
}

fn main() {
    match main_wrapped() {
        Ok(_) => {}
        Err(e) => error!("{:?}", e),
    }
}

fn main_wrapped() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("debug"));
    tracy_client::Client::start();

    let conn = xwin::Connection::connect()?;
    let xcb = &conn;

    //let window = Arc::new(conn.create_window()?);
    //let renderer = vulkan::Renderer::for_xwin(window.clone())?;
    //let over = Arc::new(AtomicBool::new(false));

    let setup = xcb.get_setup();
    info!("Bit order: {:?}", setup.bitmap_format_bit_order());
    info!("Scanline pad: {:?}", setup.bitmap_format_scanline_pad());
    info!("Scanline unit: {:?}", setup.bitmap_format_scanline_unit());
    info!("Byte order: {:?}", setup.image_byte_order());
    info!("Pixmap formats:");
    for fmt in setup.pixmap_formats() {
        info!("- {:?}", fmt);
    }
    let screen = setup.roots().nth(0).unwrap();
    let root_visual = screen
        .allowed_depths()
        .find(|d| d.depth() == screen.root_depth())
        .unwrap()
        .visuals()
        .iter()
        .find(|v| v.visual_id() == screen.root_visual())
        .unwrap();

    info!("Root visual: {:?}", root_visual);

    let visual_map: BTreeMap<x::Visualid, (u32, x::Visualtype)> = screen
        .allowed_depths()
        .flat_map(|d| {
            let depth = d.depth();
            d.visuals()
                .iter()
                .map(move |v| (v.visual_id(), (depth as u32, v.clone())))
        })
        .collect();

    info!("Supported visuals:");
    for (id, val) in visual_map.iter() {
        info!("- {} = {:?}", id, val);
    }

    let shm_manager = Arc::new(Mutex::new(xshm::ShmManager::new(conn.clone(), 1024)?));

    let cookie = xcb.send_request(&damage::QueryVersion {
        client_major_version: 1,
        client_minor_version: 1,
    });
    xcb.wait_for_reply(cookie)?;

    xcb.send_and_check_request(&x::ChangeWindowAttributes {
        window: conn.root_window(),
        value_list: &[x::Cw::EventMask(x::EventMask::SUBSTRUCTURE_NOTIFY)],
    })?;

    xcb.send_and_check_request(&composite::RedirectSubwindows {
        window: conn.root_window(),
        update: composite::Redirect::Automatic,
    })?;

    let cookie = xcb.send_request(&x::QueryTree {
        window: conn.root_window(),
    });
    let reply = xcb.wait_for_reply(cookie)?;

    let mut tracked_wins: BTreeMap<x::Window, TrackedXWin> = BTreeMap::new();
    let mut window_stack: Vec<x::Window> = Vec::new();
    let (render_sender, render_receiver) = mpsc::channel::<tree::TreeEvent>();

    let mut requests = Vec::with_capacity(reply.children().len());
    for window in reply.children().iter().cloned() {
        let r1 = xcb.send_request(&x::GetWindowAttributes { window });
        let r2 = xcb.send_request(&x::GetGeometry {
            drawable: x::Drawable::Window(window),
        });
        requests.push((r1, r2));
    }

    for (window, (r1, r2)) in reply.children().iter().zip(requests.into_iter()) {
        let attributes = xcb.wait_for_reply(r1)?;
        let geometry = xcb.wait_for_reply(r2)?;
        let mut obj = TrackedXWin {
            position: (geometry.x() as i32, geometry.y() as i32),
            size: (geometry.width() as u32, geometry.height() as u32),
            class: attributes.class(),
            map_state: attributes.map_state(),
            visual: visual_map.get(&attributes.visual()).unwrap().clone(),
            damage: None,
            surface: None,
        };
        if obj.class == x::WindowClass::InputOutput && obj.map_state == x::MapState::Viewable {
            obj.damage = Some(conn.create_damage(*window)?);
            let surface = XSurfaceImage::for_window(
                conn.clone(),
                shm_manager.clone(),
                *window,
                (geometry.width(), geometry.height()),
            )?;
            obj.surface = Some(surface.clone());
            let _ = render_sender.send(TreeEvent::UpdateRenderObject(
                window.resource_id() as u64,
                TreeRenderObject {
                    position: [obj.position.0, obj.position.1, window_stack.len() as i32],
                    size: [obj.size.0, obj.size.1],
                },
            ));
            let _ = render_sender.send(TreeEvent::UpdateSurface(
                window.resource_id() as u64,
                surface.into(),
            ));
        }
        tracked_wins.insert(*window, obj);
        window_stack.push(*window);
    }

    let window = Arc::new(conn.create_window()?);

    debug!("Rendering to X window {}", window.xid());
    let mut ignore_xid = window.xid();

    let renderer = vulkan::Renderer::for_xwin(window.clone(), render_receiver)?;

    let _rg = renderer.run();

    loop {
        match xcb.poll_for_event() {
            Ok(None) => sleep(Duration::from_millis(1)),
            Ok(Some(ev)) => {
                let span = span!("ProcessXcbEvent");
                span.emit_text(&format!("{:?}", ev));
                match ev {
                    // SubstructureNotify events for tracked windows
                    xcb::Event::X(x::Event::CreateNotify(ev)) => {
                        debug!("CreateNotify: {:?}", ev);
                        let r1 = xcb.send_request(&x::GetWindowAttributes {
                            window: ev.window(),
                        });
                        let attributes = match xcb.wait_for_reply(r1) {
                            Ok(a) => a,         // yay!,
                            Err(_) => continue, // oh no
                        };
                        let mut obj = TrackedXWin {
                            position: (ev.x() as i32, ev.y() as i32),
                            size: (ev.width() as u32, ev.height() as u32),
                            class: attributes.class(),
                            map_state: attributes.map_state(),
                            visual: visual_map.get(&attributes.visual()).unwrap().clone(),
                            damage: None,
                            surface: None,
                        };
                        window_stack.push(ev.window());

                        if obj.map_state != x::MapState::Unmapped {
                            warn!(
                                "Uhm??? Window {} started in {:?} state",
                                ev.window().resource_id(),
                                obj.map_state
                            );
                            if obj.class == x::WindowClass::InputOutput
                                && obj.map_state == x::MapState::Viewable
                            {
                                obj.damage = Some(conn.create_damage(ev.window())?);
                                debug!("Visual: {:?}", obj.visual);
                                let surface = XSurfaceImage::for_window(
                                    conn.clone(),
                                    shm_manager.clone(),
                                    ev.window(),
                                    (ev.width(), ev.height()),
                                )?;
                                obj.surface = Some(surface.clone());
                                let _ = render_sender.send(TreeEvent::UpdateRenderObject(
                                    ev.window().resource_id() as u64,
                                    TreeRenderObject {
                                        position: [
                                            obj.position.0,
                                            obj.position.1,
                                            (window_stack.len() - 1) as i32,
                                        ],
                                        size: [obj.size.0, obj.size.1],
                                    },
                                ));
                                let _ = render_sender.send(TreeEvent::ZUpdate(
                                    window_stack
                                        .iter()
                                        .map(|x| x.resource_id() as u64)
                                        .collect(),
                                ));
                                let _ = render_sender.send(TreeEvent::UpdateSurface(
                                    ev.window().resource_id() as u64,
                                    surface.into(),
                                ));
                            }
                        }
                        tracked_wins.insert(ev.window(), obj);
                    }
                    xcb::Event::X(x::Event::DestroyNotify(ev)) => {
                        debug!("DestroyNotify: {:?}", ev);
                        if let Some(old_window) = tracked_wins.remove(&ev.window()) {
                            if old_window.map_state == x::MapState::Viewable
                                && old_window.class == x::WindowClass::InputOutput
                            {
                                let _ = render_sender.send(TreeEvent::RemoveRenderObject(
                                    ev.window().resource_id() as u64,
                                ));
                            }
                        }
                        window_stack.retain(|e| *e != ev.window());
                    }
                    xcb::Event::X(x::Event::UnmapNotify(ev)) => {
                        debug!("UnmapNotify: {:?}", ev);
                        if let Some(obj) = tracked_wins.get_mut(&ev.window()) {
                            obj.map_state = x::MapState::Unmapped;
                            obj.damage = None;
                            obj.surface = None;
                            if obj.class == x::WindowClass::InputOutput
                                && ev.window().resource_id() != ignore_xid
                            {
                                let _ = render_sender.send(TreeEvent::RemoveRenderObject(
                                    ev.window().resource_id() as u64,
                                ));
                            }
                        }
                    }
                    xcb::Event::X(x::Event::MapNotify(ev)) => {
                        debug!("MapNotify: {:?}", ev);
                        if let Some(obj) = tracked_wins.get_mut(&ev.window()) {
                            let z_pos =
                                window_stack.iter().position(|e| *e == ev.window()).unwrap();

                            if obj.class == x::WindowClass::InputOutput
                                && obj.map_state != x::MapState::Viewable
                                && ev.window().resource_id() != ignore_xid
                            {
                                debug!("Visual: {:?}", obj.visual);
                                let surface = XSurfaceImage::for_window(
                                    conn.clone(),
                                    shm_manager.clone(),
                                    ev.window(),
                                    (obj.size.0 as u16, obj.size.1 as u16),
                                )?;
                                obj.surface = Some(surface.clone());
                                let _ = render_sender.send(TreeEvent::UpdateRenderObject(
                                    ev.window().resource_id() as u64,
                                    TreeRenderObject {
                                        position: [obj.position.0, obj.position.1, z_pos as i32],
                                        size: [obj.size.0, obj.size.1],
                                    },
                                ));
                                let _ = render_sender.send(TreeEvent::ZUpdate(
                                    window_stack
                                        .iter()
                                        .map(|x| x.resource_id() as u64)
                                        .collect(),
                                ));
                                let _ = render_sender.send(TreeEvent::UpdateSurface(
                                    ev.window().resource_id() as u64,
                                    surface.into(),
                                ));
                            }
                            obj.map_state = x::MapState::Viewable;
                        }
                    }
                    xcb::Event::X(x::Event::ReparentNotify(ev)) => {
                        debug!("ReparentNotify: {:?}", ev);
                        if ev.window().resource_id() == window.xid()
                            && ev.parent().resource_id() != screen.root().resource_id()
                        {
                            ignore_xid = ev.parent().resource_id();
                            if let Some(obj) = tracked_wins.get_mut(&ev.parent()) {
                                if obj.class == x::WindowClass::InputOutput
                                    && obj.map_state == x::MapState::Viewable
                                {
                                    let _ = render_sender.send(TreeEvent::RemoveRenderObject(
                                        ev.window().resource_id() as u64,
                                    ));
                                }
                            }
                        }
                    }
                    xcb::Event::X(x::Event::ConfigureNotify(ev)) => {
                        tracy_client::Client::running()
                            .unwrap()
                            .message("xcb:Events:ConfigureNotify", 0);
                        debug!("ConfigureNotify: {:?}", ev);
                        if let Some(obj) = tracked_wins.get_mut(&ev.window()) {
                            obj.position = (ev.x() as i32, ev.y() as i32);
                            let new_size: (u32, u32) = (ev.width() as u32, ev.height() as u32);
                            let mut new_surface = None;
                            if obj.size != new_size {
                                debug!("Resetting XSurface");
                                debug!("Visual: {:?}", obj.visual);
                                match XSurfaceImage::for_window(
                                    conn.clone(),
                                    shm_manager.clone(),
                                    ev.window(),
                                    (new_size.0 as u16, new_size.1 as u16),
                                ) {
                                    Err(e) => {
                                        error!("Failed to create surface: {}", e);
                                        obj.surface = None;
                                    }
                                    Ok(s) => {
                                        new_surface = Some(s.clone());
                                        obj.surface = Some(s);
                                    }
                                }
                            }
                            obj.size = new_size;
                            let new_z_pos;
                            if ev.above_sibling().is_none() {
                                window_stack.retain(|e| *e != ev.window());
                                window_stack.insert(0, ev.window());
                                new_z_pos = 0;
                            } else {
                                window_stack.retain(|e| *e != ev.window());
                                let maybe_sibling_idx =
                                    window_stack.iter().position(|e| *e == ev.above_sibling());
                                if let Some(sibling_idx) = maybe_sibling_idx {
                                    window_stack.insert(sibling_idx + 1, ev.window());
                                    new_z_pos = sibling_idx + 1;
                                } else {
                                    warn!(
                                        "Asked to position window {} above window {}, but we don't have any recollection of window {}",
                                        ev.window().resource_id(),
                                        ev.above_sibling().resource_id(),
                                        ev.above_sibling().resource_id()
                                    );
                                    new_z_pos = window_stack
                                        .iter()
                                        .position(|e| *e == ev.window())
                                        .unwrap();
                                }
                            }

                            if obj.map_state == x::MapState::Viewable
                                && obj.class == x::WindowClass::InputOutput
                                && ev.window().resource_id() != ignore_xid
                            {
                                let _ = render_sender.send(TreeEvent::UpdateRenderObject(
                                    ev.window().resource_id() as u64,
                                    TreeRenderObject {
                                        position: [
                                            obj.position.0,
                                            obj.position.1,
                                            new_z_pos as i32,
                                        ],
                                        size: [obj.size.0, obj.size.1],
                                    },
                                ));
                                let _ = render_sender.send(TreeEvent::ZUpdate(
                                    window_stack
                                        .iter()
                                        .map(|x| x.resource_id() as u64)
                                        .collect(),
                                ));
                                if let Some(surface) = new_surface {
                                    let _ = render_sender.send(TreeEvent::UpdateSurface(
                                        ev.window().resource_id() as u64,
                                        surface.into(),
                                    ));
                                }
                            }
                        }
                    }
                    xcb::Event::X(x::Event::GravityNotify(_)) => {
                        // impossible to receive?
                    }
                    xcb::Event::X(x::Event::CirculateNotify(ev)) => {
                        if tracked_wins.contains_key(&ev.window()) {
                            window_stack.retain(|e| *e != ev.window());
                            if ev.place() == x::Place::OnTop {
                                window_stack.push(ev.window());
                            } else {
                                window_stack.insert(0, ev.window());
                            }
                            let _ = render_sender.send(TreeEvent::ZUpdate(
                                window_stack
                                    .iter()
                                    .map(|x| x.resource_id() as u64)
                                    .collect(),
                            ));
                        }
                    }
                    // End of SubstructureNotify events for tracked windows
                    xcb::Event::X(x::Event::ClientMessage(msg)) => {
                        if let x::ClientMessageData::Data32([atom, ..]) = msg.data() {
                            if atom == window.wm_delete_window_atom() {
                                info!("Window closed");
                                break;
                            }
                        }
                    }
                    xcb::Event::Damage(damage::Event::Notify(ev)) => {
                        tracy_client::Client::running()
                            .unwrap()
                            .message("xcb:Events:DamageNotify", 0);
                        let maybe_obj = tracked_wins.iter().find(|(_, obj)| {
                            obj.damage.as_deref().map(|d| *d) == Some(ev.damage())
                        });
                        if let Some((id, obj)) = maybe_obj {
                            //debug!("Window {} damaged, {:?}", id.resource_id(), ev.geometry());
                            xcb.send_request(&damage::Subtract {
                                damage: *obj.damage.as_deref().unwrap(),
                                repair: xfixes::REGION_NONE,
                                parts: xfixes::REGION_NONE,
                            });
                            let _ = xcb.flush();
                            if let Some(surface) = &obj.surface {
                                match surface.update() {
                                    Err(e) => error!(
                                        "Failed to update surface for window {}: {}",
                                        id.resource_id(),
                                        e
                                    ),
                                    Ok(_) => {}
                                }
                            }
                        }
                    }
                    ev => {
                        debug!("Unhandled XCB event: {:?}", ev);
                    }
                }
            }
            Err(xcb::Error::Protocol(p)) => {
                warn!("Ignored X protocol error: {:?}", p);
            }
            Err(e) => {
                error!("Failed to wait for XCB events: {:?}", e);
                break;
            }
        }
    }

    return Ok(());
}
